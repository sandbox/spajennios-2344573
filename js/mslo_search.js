/**
 * Drupal.behaviors.searchResults()
 *
 * @description
 *  Applies masonry layout for search results.
 *  Ajax loads next page of results.
 *  Fires omniture.
 *  Clones ads for mobile view.
 *  Applies events to sort filter
 *
 * @requires
 *  masonry jquery plugin
 */
Drupal.behaviors.searchResults = function(context) {
  var $pageSearch = $('body.page-search'),
      $container = $('.search-results'),
      item = '.result-box',
      nav = '.pager',
      next = '.pager-next a',
      colWidth = 198,
      gutWidth = 25,
      fadeTime = 700,
      mobileImgWidth = 130,
      mobileImgHeight = 162,
      mobileImgHeightVideo = 72,
      isMobile = false,
      isDesktop = true,
      infiniteScrollOn = true;

  // Bounce early.
  if ($pageSearch.length < 1 ||
      $container.length < 1 ||
      !jQuery().masonry) { return; }

  // Sort form.
  $('.li-sort .js-active').bind('click', function(e) {
    $(this).parents('.li-sort').toggleClass('visible');
    e.preventDefault();
  });

  if (Drupal.settings.viewportWidth < 551) {
    colWidth = 140;
    gutWidth = 10;
    isMobile = true;
    isDesktop = false;
  }

  $.each($(item), function(key, value) {
    var height,
        width,
        dataHeight = $(value).find('img').attr('data-img-height'),
        dataWidth = $(value).find('img').attr('data-img-width');

    // Assign default image placeholder size.
    height = dataHeight || null;
    width = dataWidth || null;

    // Assign different placeholder size for mobile screen.
    if (isMobile) {
      width = (dataWidth) ? mobileImgWidth : null;
      height = (dataHeight) ? mobileImgHeight : null;
      if ($(value).hasClass('js-search-video')) {
        height = (dataHeight) ? mobileImgHeightVideo : null;
      }
    }

    // Set placeholder inline css width and height.
    $(value).find('.js-search-image').css({'width' : width, 'height' : height});
  });

  $container.imagesLoaded(function() {
    $container.masonry({
      itemSelector: item,
      //columnWidth: colWidth,
      gutterWidth: gutWidth,
      isFitWidth: true
    });
  });
  $(item).find('img').fadeIn(fadeTime);

  if (infiniteScrollOn && jQuery().infinitescroll) {
  var scrollCount = 0;
  $container.infinitescroll({
    state: {
      currPage: 0
    },
    navSelector : nav, // selector for the paged navigation
    nextSelector : next, // selector for the NEXT link (to page 2)
    itemSelector : item, // selector for all items you'll retrieve
    bufferPx     : 30,
    localMode    : true,
    pathParse    : function() {
      var filters = '';
      // Grab filters if they exist.
      if (typeof Drupal.settings.mslo_search_filters !== "undefined") {
        filters = Drupal.settings.mslo_search_filters;
      }
      else if (typeof document.URL.split('?')[1] !== "undefined"){
        filters = '&' + document.URL.split('?')[1];
      }
      var path = ['?page=', filters];
      return path;
    },
    loading: {
      finishedMsg: '<div class="load-more landing-title">No more results.</div>',
      img: Drupal.settings.mslo_themehelper.loader_image,
      msgText: '<div class="load-more landing-title">Loading more results...</div>',
      speed: 'fast'
      }
    },
    function(newElements) {
      scrollCount++;
      var $newElems,
          $anchor = $('.search-results .dart-tag-27:last'), // where ad will be appended to
          $anchorMobile = $('.search-results .dart-tag-26:last'), // where mobile ad will be appended to
          position = 'append',  // function to use
          $ad = $('.dart-tag-27:first .js-ad-wrapper'), // persistent ad iframe
          $adMobile = $('.dart-tag-26:first .js-ad-wrapper'); // mobile ad iframe

      $newElems = $(newElements);

      $.each($newElems, function(key, value) {
        var height,
            width,
            dataHeight = $(value).find('img').attr('data-img-height'),
            dataWidth = $(value).find('img').attr('data-img-width');

        // Assign default image placeholder size.
        height = dataHeight || null;
        width = dataWidth || null;

        // Assign different placeholder size for mobile screen.
        if (isMobile) {
          width = (dataWidth) ? mobileImgWidth : null;
          height = (dataHeight) ? mobileImgHeight : null;
          if ($(value).hasClass('js-search-video')) {
            height = (dataHeight) ? mobileImgHeightVideo : null;
          }
        }

        // Set inline width and height.
        $(value).find('.js-search-image').css({'width' : width, 'height' : height});
      });

      // Append ajax loaded elements.
      $container.masonry('appended', $newElems, true);

      // Fire omniture.
      var omniture_obj = {
        trackVars: 'eVar1,prop8',
        trackEvents: 'event4',
        data: 'search_scroll_' + String(scrollCount),
        linkname: 'search_scroll',
        pageUpdate: 'page' + String(scrollCount + 1)
      };
      Drupal.mslo.fire_omniture(omniture_obj);
      $.each($('.om-section'), Drupal.mslo.omniture_section);

      // Clone and place the new ads with matching ordernumbers
      var newOrderNumber = Math.floor(Math.random()*999999999);
      var desktop_obj = {
        anchor: $anchor,
        position: position,
        ad: $ad,
        incrementer: scrollCount,
        ordnum: newOrderNumber
      }
      var mobile_obj = {
        anchor: $anchorMobile,
        position: position,
        ad: $adMobile,
        incrementer: scrollCount,
        ordnum: newOrderNumber
      }
      Drupal.mslo_ads.cloneAd(desktop_obj);

      // Brittle here. Only clone mobile ad when 728 disappears.
      if (Drupal.settings.viewportWidth <= 900) {
        Drupal.mslo_ads.cloneAd(mobile_obj);
      }

      // As images are loaded, fade them in.
      $newElems.find('img').load(function() {
        $(this).fadeIn(fadeTime);
      });
    }
  ); // end infinitescroll
  } // end if infinitescroll turned on
};
