Drupal.behaviors.node_search_redirect = function(context) {
  if (window.location.href.indexOf('/search/results') > 0
    && window.location.href.indexOf('/search/results/mlt') == 0) {
    return;
  }

  // @TODO: Take over AM search.
  var $searchFormAM = $('#search-theme-form');
  $searchFormAM.submit(function(event) {
    $textInput = $('#edit-search-theme-form-1');
    if ($textInput.val() !== '') {
      var $jsSearch = $('#js-search');
      s.eVar21 = $textInput.val();
      Drupal.mslo.omniture_click($('#js-search-glyph'));
      var url = '/americanmade/search/' + $textInput.val();
      window.location.href = url;
    }
    event.preventDefault();
  });

  // Handles main and video search.
  $searchBlock = $('#js-header-search').find('.search-block-form');
  var originalAction = $searchBlock.attr('action');
  if (originalAction == '/search/results/nojs'
    || window.location.href.indexOf('/search/results/mlt') > 0) {
    // Disable form submit on search box.
    $searchBlock.attr('action', '#');

    // Redirect to the new search results page.
    $searchBlock.submit(function() {
      var searchTerm = '',
        searchFilter = '';

      // Add filter to video search.
      if ($("input[name='video_filter']").length > 0) {
        searchFilter = '&filters=video';
      }

      if ($('[name=keys]').val()) {
        searchTerm = $('[name=keys]').val();
        var url = '/search/results?keys=' + searchTerm + searchFilter;
        window.location.href = url;
      }
      return false;
    });
  }
};
