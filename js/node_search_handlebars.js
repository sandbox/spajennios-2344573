Drupal.node_search = {};
Drupal.settings.node_search.debug = false;
// DOCUMENT READY =================================================

// @TODO: (DLIN) Make this not global
// @TODO: (JREYNOLDS) Note that Google Publisher Tags, which we will change to at some
// point, maybe soon, does away with the concept of tile numbers. Should
// we try to build in some future-proofing here? Or accept this might be
// temporary?
var tileNum = 1;

Drupal.behaviors.node_search_load = function(context) {
  // Disable form submit on search box.
  $('.search-block-form').attr('action', '#');

  // Initialize json content.
  if (Drupal.settings.node_search.mltSolrQuery) {
    Drupal.node_search.renderMltContent();
  }
  else {
    Drupal.node_search.renderContent();
    Drupal.node_search.bindPager();
  }
};

// FUNCTIONS ===================================================

// Fallback to old search.
Drupal.node_search.fallback = function(searchTerm) {
  var url = '/search/apachesolr_search/';
  if (searchTerm != '') {
    url += searchTerm;
  }
  window.location.href = url;
};

// Parse the querystring and return all the values.
Drupal.node_search.getUrlVars = function(url) {
  var vars = [], hash, hashes;
  if (!url) {
    hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
  }
  else {
    hashes = url.slice(url.indexOf('?') + 1).split('&');
  }

  for(var i = 0; i < hashes.length; i++) {
    hash = hashes[i].split('=');
    if (hash[0] == 'keys'
      || hash[0] == 'page'
      || hash[0] == 'filters'
      || hash[0] == 'nid') {

      // Strip the paging # out of the value.
      if (hash[1].indexOf('#') > 1) {
        val = hash[1].slice(0, hash[1].indexOf('#'));
      }
      else {
        val = hash[1];
      }

      vars.push(hash[0]);
      vars[hash[0]] = val;
    }
  }
  return vars;
};

// Builds the URL for a basic search request.
Drupal.node_search.parseUrl = function(page, keys, filters) {
  Drupal.settings.node_search.solrQuery = '?page=' + page
    + '&q=' + keys
    + '&spellcheck.q=' + keys
    + '&fq=' + Drupal.node_search.getFQData(filters),
    + Drupal.settings.node_search.solrQuery;

  return Drupal.settings.node_search.nodeServer + Drupal.settings.node_search.solrQuery;
};

// Builds the Solr fq params for returning organic results.
Drupal.node_search.getFQData = function(filters) {
  var fq_type = '';
  if (filters) {
    $.each(Drupal.settings.node_search.nodeSearchFilters, function(index, value) {
      if (value.type == filters) {
        fq_type = value.qstr;
      }
    });
  }

  // If the filter provided is not valid, default to no filters experience.
  // @FIXME: If the filter provided is not valid, should we redirect to the
  // all results url?
  var fqArr = [];
  if (!filters || (fq_type == '' && filters)) {
    // Exclude legacy type Menu and AM stubs.
    fqArr.push('-type:am_nominee_stub');
    fqArr.push('-type:menu');
    // Exclude ad recipes.  We manually insert them into search results.
    fqArr.push('-type:advertiser_recipe');
  }

  fqArr.push('-ss_index_source:ecl');
   // We want exclude the ugc & ecl indexed content.
  fqArr.push('-sm_site_code:ugc');
  fqArr.push('-sm_site_code:ecl_bs');
  fqArr.push('-sm_site_code:ecl_ww');

  // Set the site hash if this is MSW search.  Omit if MS because MS results
  // include all hashes.
  if (Drupal.node_search.isThisWeddings()) {
    fqArr.push('hash:' + Drupal.settings.node_search.apachesolrSiteHash);
  }

  return fqArr.join('+AND+') + '&fq=' + fq_type;
};

// Returns the facet counts by content type.
Drupal.node_search.getFacetFieldCounts = function(searchTerm, qstr) {
  var facet_counts = '',
    defaultUrl = Drupal.node_search.parseFacetCountUrl(searchTerm, qstr);
  if (Drupal.settings.node_search.debug && typeof console != 'undefined') {
    console.log('*** Facet Field Counts Call: ' + defaultUrl);
  }

  if (qstr) {
      $.ajax({
      async: false,
      url: defaultUrl,
      dataType: 'json',
      // data: qstr,
      success: function(data) {
        facet_counts = data.facetCounts;
      }
    });
  }
  return facet_counts;
};

// Builds the URL for a facet count search request.
Drupal.node_search.parseFacetCountUrl = function(searchTerm, qstr) {
  return Drupal.settings.node_search.nodeServer
    + '?q=' + searchTerm
    + qstr;
};

// Determines if search is being called on weddings.
Drupal.node_search.isThisWeddings = function(searchTerm, qstr) {
  if (Drupal.settings.node_search.apachesolrSiteHash == 'mswprod') {
    return true;
  }
  else {
    return false;
  }
}

// Renders the first page of search.
Drupal.node_search.renderContent = function(jsonStr) {
  var defaultUrl, searchTerm, page, filters, fq;

  urlVars = Drupal.node_search.getUrlVars();
  filters = (urlVars['filters']) ? urlVars['filters'] : '';
  searchTerm = (urlVars['keys']) ? urlVars['keys'] : '';
  page = (urlVars.hasOwnProperty['page'] && parseInt(urlVars['page']) != NaN) ? parseInt(urlVars['page']) : 1;
  facetCounts = {};

  searchTermDecoded = decodeURIComponent(searchTerm.replace(/\+/g, ' '));
  defaultUrl = jsonStr || Drupal.node_search.parseUrl(page, searchTerm, filters);

  if (Drupal.settings.node_search.debug && typeof console != 'undefined') {
    console.log('*** Organic Search Call: ' + defaultUrl);
  }

  // Grab facet counts.
  // If solrQueryFacetCounts is not set, use the data from the search results request.
  if (Drupal.settings.node_search.solrQueryFacetCounts) {
    facetCounts = Drupal.node_search.getFacetFieldCounts(searchTerm, Drupal.settings.node_search.solrQueryFacetCounts);
  }

  // Get one page of ad recipes for this search.
  // One page = up to two results.
  // We need to read the page number from the current URL.
  currentUrlVars = Drupal.node_search.getUrlVars(defaultUrl);
  var adRecipes = {},
    adRecipesSize = 0,
    adRecipePosition = 0;

  // Get the ad recipes for the current page if there are no selected filters.
  if (!filters) {
    adRecipes = Drupal.node_search.getAdRecipes(currentUrlVars['page'], searchTerm);

    // The number of ad recipes returned.
    adRecipesSize = (adRecipes.data.length) ? adRecipes.data.length : 0;

    // Set the ad recipe position to zero if there are no results to show.
    adRecipePosition = (adRecipesSize > 0) ? Drupal.settings.node_search.adRecipePosition : 0;

    // Adjust the organic recipe results page size to accomodate for the ads.
    pageSize = Drupal.settings.node_search.pageSize - adRecipesSize;
  }

  // Grab search results from url or as jsonStr passed into the function.
  $.ajax(defaultUrl)
  .fail(function(jqXHR, textStatus, errorThrown) {
    Drupal.node_search.fallback(searchTerm);
    return false;
  })
  .done(function(data) {
    var dataWithAds = [],
        adPosition = Drupal.settings.node_search.adPosition,
        adRecipeIndex = 0,
        showAd = true;

    if (typeof facetCounts != 'undefined') {
      if (!facetCounts.hasOwnProperty('facet_fields')) {
        facetCounts = data.facetCounts;
      }
    }
    for (var i = 0, j = data.data.length; i < j; i++) {

      // Push ads into new json returned data set.
      if (i % adPosition == adPosition - 1 && showAd) {
        // Build the Ad.
        dataWithAds.push(Drupal.node_search.getAdObject(tileNum, searchTerm));

        // We only want to show one ad per page.
        showAd = false;
        tileNum++;
      }

      // Push an advertiser recipe if we have one.
      if (adRecipesSize > 0
        && i % adRecipePosition == adRecipePosition - 2
        && adRecipesSize > adRecipeIndex) {

        // Add sponsored banner / flag.
        adRecipes.data[adRecipeIndex].sponsored = true;
        // @FIXME: Remove special characters from the title (&amp;amp)
        adRecipes.data[adRecipeIndex].titleEncoded = adRecipes.data[adRecipeIndex].title;

        dataWithAds.push(adRecipes.data[adRecipeIndex]);
        adRecipeIndex++;
      }

      Drupal.node_search.renderBadges(data, i);
      if (data.data[i].type == 'video' || data.data[i].type == 'video_playlist' || data.data[i].type == 'hub') {
        data.data[i].videoGlyph = true;
      }

      // Hide certain content types from display.
      $.each(Drupal.settings.node_search.hiddenContentTypes, function(index, value) {
        if (value == data.data[i].type) {
          data.data[i].typeName = '';
        }
      });

      data.data[i].titleEncoded = data.data[i].title;

      // Add search term to node object to generate MLT link.
      data.data[i].searchTerm = searchTerm;
      // Recreate data set.
      dataWithAds.push(data.data[i]);
    }

    // Add the search term to the filter data.
    var filterData = [],
      selectedFilter = '';

    $.each(Drupal.settings.node_search.nodeSearchFilters, function(index, value) {
      var d = value;
      d.searchTerm = searchTerm;

      // If we are displaying filters,
      // disable each filter if it does not have data.
      if (typeof facetCounts != 'undefined') {
        $.each(facetCounts.facet_fields.type_name, function(key, value) {
          if (d.type.toLowerCase() == key.toLowerCase() && value > 0) {
            d.enabled = true;
          }
        });
      }

      filterData.push(d);
      if (filters == d.qstr) {
        selectedFilter = d.display.toLowerCase();
      }
    });

    var spellcheck = '';
    if (data.spellcheck !== undefined
      && data.spellcheck.hasOwnProperty('suggestions')
      && data.spellcheck.suggestions.hasOwnProperty(searchTermDecoded)) {
      spellcheck = String(data.spellcheck.suggestions[searchTermDecoded].suggestion);
    }

    var isFirstPage = (parseInt(data.pagination.current) == 1) ? true : false;

    // Construct data object to send to handlebars.
    var dataObj = {
      contentArr: dataWithAds,
      searchTerm: searchTerm,
      searchTermDecoded: searchTermDecoded,
      numResults: data.pagination.totalResult,
      currentPageNum: data.pagination.current,
      nextPageUrl: '',
      filters: filterData,
      firstPage: isFirstPage,
      spellcheck: spellcheck,
      contentList: Drupal.settings.node_search.content_list,
      selectedFilter: selectedFilter
    };

    var currentPage = (data.pagination.current) ? parseInt(data.pagination.current) : 0,
      lastPage = (data.pagination.last) ? parseInt(data.pagination.last) : 0;

    if (isNaN(currentPage)) {
      dataObj.currentPageNum = 1;
    }
    if (lastPage > currentPage) {
      dataObj.nextPageNum = currentPage + 1;
      dataObj.nextPageUrl = Drupal.node_search.parseUrl(dataObj.nextPageNum, searchTerm, filters);
    }
    // Generate the HTML with handle bars based on whether it's the first page or not.
    if (Drupal.node_search.isThisWeddings()) {
      var fullTemplate = $('#search-template-msw');
    }
    else {
      var fullTemplate = $('#search-template');
    }
    var markup,
        partialTemplate = $('#search-template-partial'),
        adTemplate = $('#search-template-partial-ad'),
        filterTemplate = $('#search-template-partial-filters');

    markup = Drupal.node_search.processHandlebars(dataObj, fullTemplate, partialTemplate, adTemplate, filterTemplate);

    if (isFirstPage) {
      // Replace DOM with handlebar generated html.
      $("#js-search-wrap").html(markup);

      // Populate ads.
      var $adBlocks = $('.ad-block-search').find('iframe');
      Drupal.mslo_ads.populateAllSrc($adBlocks);

      // Apply stacking.
      Drupal.node_search.applyStacking($('#js-search-results'));
    } else {
      // Append results if we're not on the first page.
      Drupal.node_search.appendStack(markup);
    }

    // Re-activate omniture manually since DOM is ajaxed.
    // @TODO: (DLIN) Refactor omniture to be used with ajax, use .on() or singletons.
    // @TODO: (NEIL) Might be better to scope om-section to a parent section, so you're not
    // refreshing the entire page of om-sections (nav,header,footer,etc)
    $.each($('.om-links'), Drupal.mslo.om_links);
    $.each($('.om-section'), Drupal.mslo.omniture_section);

    Drupal.node_search.setFilter(filters);
    Drupal.node_search.removeLoader();
  });
};

// Process json data to generate HTML for search results.
Drupal.node_search.processHandlebars = function(data, templateObj, partialObj, adObj, filterObj) {
  // Must register partial template before setting up scoped vars.
  if (partialObj.length) {
    Handlebars.registerPartial("insides", partialObj.html());
  }
  if (adObj.length) {
    Handlebars.registerPartial("ad", adObj.html());
  }
  if (filterObj.length) {
    Handlebars.registerPartial("filters", filterObj.html());
  }
  var template = Handlebars.compile(templateObj.html()),
      templateMarkup = template(data);

  // Completely replaces HTML inside search-wrap.
  // Also try http://api.jquery.com/jQuery.data/ to removedata and add data without affecting DOM.
  return templateMarkup;
};

// FUNCTIONS for pagination ===================================================
// Bind see more pager. Appends more content on click.
Drupal.node_search.bindPager = function() {
  $('#js-search-wrap').on('click', '#js-search-next', function() {
    var $this = $(this),
        string = $('#js-search-next').attr('data-content'),
        scrollCount,
        omniture_obj;

    // Renders all search results.
    Drupal.node_search.renderContent(string);

    // Remove see more link, replace with loading message.
    $('#js-search-pager').after('<div id="js-load-more" class="header-b search-load search-msg"><span class="loader-small"></span> Loading more results...</div>');
    $('#js-search-pager').remove();

    // Fire omniture, tracks number of pages clicked to.
    scrollCount = $this.attr('data-count');
    omniture_obj = {
      trackVars: 'eVar1,prop8',
      trackEvents: 'event4',
      data: 'search_scroll_' + scrollCount,
      linkname: 'search_scroll',
      pageUpdate: 'page' + String(parseInt(scrollCount) + 1)
    };
    Drupal.mslo.fire_omniture(omniture_obj);

    // Wait a bit for the search results to render, then check for the ad blocks.
    Drupal.node_search.renderAds();
  });
};

Drupal.node_search.applyStacking = function($container) {
  var item = '.js-search-item',
      gutWidth = 10,
      animationSetting = true;

  if (Drupal.settings.viewportWidth < 551) {
    animationSetting = false;
  }

  $container.isotope({
    gutterWidth: gutWidth,
    transformsEnabled: animationSetting,
    animationEngine: 'jquery',
  });
};

Drupal.node_search.appendStack = function(templateMarkup) {
  // Filter out descendant text nodes in the DOM.
  var $filteredmarkup = $(templateMarkup).filter('.js-search-item'),
      $pagerNew = $(templateMarkup).filter('#js-search-pager'),
      $searchResults = $('#js-search-results');

  $searchResults.isotope().append($filteredmarkup).isotope('appended', $filteredmarkup);
  $searchResults.after($pagerNew);
};
// END FUNCTIONS for pagination ===================================================

// FUNCTIONS for manipulating search results ===================================================

Drupal.node_search.removeLoader = function() {
  $('#js-load-more').remove();
};

Drupal.node_search.renderAds = function() {
  // The time can be adjusted.
  // @TODO: Gather mobile requirements? Is mobile different from desktop?
  // Currently serving 2 ads per page load
  setTimeout(function() {
    // Populate new ads
    var secondToLastAd = $('.ad-block-search').length - 2,
        $lastAdBlock1 = $('.ad-block-search:last').find('iframe'),
        $lastAdBlock2 = $('.ad-block-search').eq(secondToLastAd).find('iframe');

    $lastAdBlock1.addClass('ad-1');
    $lastAdBlock2.addClass('ad-2');
    Drupal.mslo_ads.populateAllSrc($lastAdBlock1);
    Drupal.mslo_ads.populateAllSrc($lastAdBlock2);
  }, 1500);
};

// Set the active filter.
Drupal.node_search.setFilter = function(currentFilter) {
  // @FIXME: I think we can disable filters here based on the data returned
  // from solr.
  var $ul = $('#search-filters'),
    filter = '';

  $ul.children().each(function(index) {
    if (index % 2 == 1) {
      $(this).addClass('item-even');
    }
    else {
      $(this).addClass('item-odd');
    }
    // $(this).last().addClass('last');
    if (currentFilter) {
      $.each(Drupal.settings.node_search.nodeSearchFilters, function( index, value ) {
        if (currentFilter == value.type) {
          filter = value.type;
        }
      });
    } else {
      filter = 'all';
    }
    $ul.children().last().addClass('last');

    // If the selector previously exists on a filter, remove it.
    $(this).children('a').children().remove('.active-arrow');

    // Select the chosen filter.
    Drupal.node_search.toggleFilter($(this), filter);
  });
};

Drupal.node_search.toggleFilter = function($theFilter, currentFilter) {
  var $activeArrow = $('<span>').addClass('active-arrow');
  if ($theFilter.attr('data-content') == currentFilter) {
    $theFilter.children('a').addClass('active');
    $theFilter.children('a').children('.filter-link').addClass('active');
    $theFilter.children('a').append($activeArrow);
  }
  else {
    $theFilter.children('a').removeClass('active');
    $theFilter.children('a').children('.filter-link').removeClass('active');
    $theFilter.children('a').children().remove('.active-arrow');
  }
};

// Adds badges to search results.
Drupal.node_search.renderBadges = function(data, i) {
  // Add sponsored banner / flag.
  if (data.data[i].type == 'advertiser_recipe') {
    data.data[i].sponsored = true;
  }
  else if (data.data[i].type == 'recipe') {
    if (data.data[i].badge_martha_bakes) {
      badge_key = 'mb';
    }
    else if (data.data[i].badge_marthas_favorite) {
      badge_key = 'mf';
    }
    else if (data.data[i].badge_under_thirty_mins) {
      badge_key = 'utm';
    }
    else {
      badge_key = '';
    }
    if (badge_key != '') {
      data.data[i].badge = true;
      data.data[i].badge_desc = Drupal.settings.node_search.nodeSearchBadges[badge_key].badge_desc;
      data.data[i].badge_code = badge_key;
      data.data[i].badge_src  = Drupal.settings.node_search.nodeSearchBadges[badge_key].badge_src;
    }
  }
};

// Returns an ad object.
Drupal.node_search.getAdObject = function(tileNum, searchTerm) {
  var ad = {};
  ad.ord = Math.floor(Math.random()*999999999);
  ad.tile = tileNum;
  ad.isAd = true;
  ad.adSrc = 'http://ad.doubleclick.net/adi/mso.site/search;position=300x250_iframe;sz=300x250;tile=' + ad.tile + ';pageid=;galleryid=;content_type=search;brand=;refer=;vid=false;kw=' + encodeURIComponent(searchTerm) + ';asi=G10942_10017;asi=D08734_72078;asi=G10942_10046;asi=G10942_0;;u=channel|300x250_iframe||true|||||tran|true||||;ord=' + ad.ord;
  ad.adClass = 'js-ad-wrapper dyperf dyMonitor';
  ad.adDataAdid = '18950||769|||';
  return ad;
};
// END FUNCTIONS for manipulating search results ===================================================

// FUNCTIONS for MLT ===================================================
// Builds the URL for a MLT search request.
Drupal.node_search.parseMltUrl = function(nid) {
  return Drupal.settings.node_search.nodeServer
    + '?q=id:' + Drupal.settings.node_search.apachesolrSiteHash + '/node/' + nid
    + Drupal.settings.node_search.mltSolrQuery;
};

// Renders one page of MLT content.
Drupal.node_search.renderMltContent = function() {
  var defaultUrl, searchTerm, nid;

  urlVars = Drupal.node_search.getUrlVars();
  nid = (urlVars['nid']) ? urlVars['nid'] : '';
  searchTerm = (urlVars['keys']) ? urlVars['keys'] : '';
  facetCounts = {};

  searchTermDecoded = decodeURIComponent(searchTerm.replace('+', ' '));
  defaultUrl = Drupal.node_search.parseMltUrl(nid);

  if (Drupal.settings.node_search.debug && typeof console != 'undefined') {
    console.log('*** MLT Search Call: ' + defaultUrl);
  }

  // Grab search results from url or as jsonStr passed into the function.
  $.getJSON(defaultUrl)
  .fail(function(jqXHR, textStatus, errorThrown) {
    Drupal.node_search.fallback(searchTerm);
    return false;
  })
  .done(function(data) {
    var dataWithAds = [],
        adPosition = Drupal.settings.node_search.adPosition,
        showAd = true;

    for (var i = 0, j = data.data.length; i < j; i++) {

      // Push ads into new json returned data set.
      if (i % adPosition == adPosition - 1 && showAd) {
        // Build the Ad.
        dataWithAds.push(Drupal.node_search.getAdObject(tileNum, searchTerm));

        // We only want to show one ad per page.
        showAd = false;
        tileNum++;
      }

      Drupal.node_search.renderBadges(data, i);

      if (data.data[i].type == 'video' || data.data[i].type == 'video_playlist' || data.data[i].type == 'hub') {
        data.data[i].videoGlyph = true;
      }

      // Hide certain content types from display.
      $.each(Drupal.settings.node_search.hiddenContentTypes, function(index, value) {
        if (value == data.data[i].type) {
          data.data[i].typeName = '';
        }
      });

      // @TODO: Remove special characters from the title (&amp;amp)
      data.data[i].titleEncoded = data.data[i].title;

      // Add search term to node object to generate MLT link.
      data.data[i].searchTerm = searchTerm;

      // Recreate data set.
      dataWithAds.push(data.data[i]);
    }
    var searchNode = Drupal.node_search.getMltSearchNode(nid),
      searchNodeTitle = '';

    if (typeof searchNode != 'undefined') {
      if (typeof searchNode.data == 'object'
        && searchNode.data.length > 0
        && typeof searchNode.data[0].title == 'string') {
        searchNodeTitle = searchNode.data[0].title;
      }
    }
    // Construct data object to send to handlebars.
    var dataObj = {
      contentArr: dataWithAds,
      searchTerm: searchTerm,
      searchNodeTitle: searchNodeTitle,
      firstPage: true,
      contentList: Drupal.settings.node_search.content_list,
    };

    // Generate the HTML with handle bars based on whether it's the first page or not.
    var markup,
        fullTemplate = $('#search-template'),
        partialTemplate = $('#search-template-partial'),
        adTemplate = $('#search-template-partial-ad');

    markup = Drupal.node_search.processHandlebarsMlt(dataObj, fullTemplate, partialTemplate, adTemplate);

    // Replace DOM with handlebar generated html.
    $("#js-search-wrap").html(markup);

    // Populate ads.
    var $adBlocks = $('.ad-block-search').find('iframe');
    Drupal.mslo_ads.populateAllSrc($adBlocks);

    // Apply stacking.
    Drupal.node_search.applyStacking($('#js-search-results'));

    // Re-activate omniture manually since DOM is ajaxed.
    // @TODO: Refactor omniture to be used with ajax, use .on() or singletons.
    $.each($('.om-links'), Drupal.mslo.om_links);
    $.each($('.om-section'), Drupal.mslo.omniture_section);

    Drupal.node_search.removeLoader();
  });
};

// Process json data to generate HTML for MLT results.
Drupal.node_search.processHandlebarsMlt = function(data, templateObj, partialObj, adObj) {
  // Must register partial template before setting up scoped vars.
  if (partialObj.length) {
    Handlebars.registerPartial("insides", partialObj.html());
  }
  if (adObj.length) {
    Handlebars.registerPartial("ad", adObj.html());
  }
  var template = Handlebars.compile(templateObj.html()),
      templateMarkup = template(data);

  // Completely replaces HTML inside search-wrap.
  // Also try http://api.jquery.com/jQuery.data/ to removedata and add data without affecting DOM.
  return templateMarkup;
};

// Returns the title of the node we are using for MLT.
Drupal.node_search.getMltSearchNode = function(nid) {
  mltUrl = Drupal.settings.node_search.nodeServer
    + '?fq=id:' + Drupal.settings.node_search.apachesolrSiteHash + '/node/' + nid;

  if (Drupal.settings.node_search.debug && typeof console != 'undefined') {
    console.log('*** MLT Node Lookup Call: ' + mltUrl);
  }

  // Grab the ad recipe search results
  var result = {
    data: {}
  };

  $.ajax({
    async: false,
    url: mltUrl,
    dataType: "json",
    success: function(data){
      result = data;
    }
  });
  return result;
};
// END FUNCTIONS for MLT ===================================================

// FUNCTIONS for Sponsored Recipes ===================================================
// Builds the Solr fq params for returning ad recipes.
Drupal.node_search.getAdRecipeFQData = function() {
  var fqArr = [],
  isValidFilter = false;

  fqArr.push('type:advertiser_recipe');
  fqArr.push('-ss_index_source:ecl');
  fqArr.push('-sm_site_code:ugc');
  fqArr.push('-sm_site_code:ecl_bs');
  fqArr.push('-sm_site_code:ecl_ww');

  return fqArr.join('+AND+');
};

Drupal.node_search.getAdRecipes = function(page, keys) {
  Drupal.settings.node_search.adRecipeSolrQuery =
    '?page=' + page
    + '&q=' + keys
    + '&fq=' + Drupal.node_search.getAdRecipeFQData()
    + Drupal.settings.node_search.adRecipeSolrQuery;

  if (Drupal.settings.node_search.debug && typeof console != 'undefined') {
    console.log('*** Sponsored Recipe Call: ' + Drupal.settings.node_search.adRecipeSolrQuery);
  }

  // Grab the ad recipe search results
  var result = {
    data: {}
  };

  $.ajax({
    async: false,
    url: Drupal.settings.node_search.adRecipeSolrQuery,
    dataType: "json",
    success: function(data){
      result = data;
    }
  });
  return result;
};
// END FUNCTIONS for Sponsored Recipes ===================================================

