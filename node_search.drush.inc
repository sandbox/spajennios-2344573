<?php

/**
 * @file
 * node_search.drush.inc
 */


/**
 * Implements hook_drush_command().
 */
function node_search_drush_command() {
  $items = array();
  $items['node-search-remove-type-from-solr'] = array(
    'callback' => 'drush_node_search_remove_type_from_solr',
    'aliases' => array('solrrmvtype-ns'),
    'description' => 'Removes this site\'s nodes from the solr index based on the provided content type.',
    'arguments' => array(
      'type' => 'Specify a content type.',
    ),
    'examples' => array(
      'drush node-search-remove-type-from-solr' => dt('This command will remove a content type from solr'),
    ),
  );

  $items['node-search-remove-nids-from-solr'] = array(
    'callback' => 'drush_node_search_remove_nids_from_solr',
    'aliases' => array('solrrmvnid-ns'),
    'description' => 'Removes this site\'s nodes from the solr index.',
    'arguments' => array(
      'idlist' => 'Specify a comma delimited list of nids.',
    ),
    'examples' => array(
      'drush node-search-remove-nids-from-solr' => dt('This command will remove list of nids from solr'),
    ),
  );
  $items['node-search-add-nids-to-solr'] = array(
    'callback' => 'drush_node_search_add_nids_to_solr',
    'aliases' => array('solraddnid-ns'),
    'description' => 'Adds the specified nodes to the solr index.',
    'arguments' => array(
      'idlist' => 'Specify a comma delimited list of nids.',
    ),
    'examples' => array(
      'drush node-search-add-nids-to-solr' => dt('This command will add the listed node ids to solr'),
    ),
  );
  return $items;
}

function drush_node_search_remove_nids_from_solr() {
  if (!$idlist = drush_get_option('idlist')) {
    drush_log(dt('idlist not specified.'), 'error');
    return;
  }

  $arr_idlist = explode(',', $idlist);
  if ($arr_idlist) {
    $result = db_select('node', 'n')
      ->fields('n')
      ->condition('nid', $arr_idlist)
      ->execute();
    $row_count = $result->rowCount();

    if (! drush_confirm(
      dt('Are you sure? This is a destructive command that will remove @count from the solr index.',
        array('@count' => format_plural($row_count, '1 node', '@count nodes'))
      ))) {
      return drush_user_abort();
    }

    if ($result) {
      while($record = $result->fetchObject()) {
        _drush_node_search_remove_from_solr($record->nid, 'node_search');
      }
    }
  } else {
    drush_log(dt('idlist must be a comma delimited list of nids.'), 'error');
    return;
  }
}

function drush_node_search_remove_type_from_solr() {
  if (!$content_type = drush_get_option('type')) {
    drush_log(dt('type not specified.'), 'error');
    return;
  }

  $result = db_select('node', 'n')
    ->fields('n')
    ->condition('type', $content_type)
    ->execute();
  $row_count = $result->rowCount();
  if (! drush_confirm(
    dt('Are you sure? This is a destructive command that will remove @count from the solr index.',
        array('@count' => format_plural($row_count, '1 node', '@count nodes'))
    )
  )) {
    return drush_user_abort();
  }

  if ($result) {
    while($record = $result->fetchObject()) {
      _drush_node_search_remove_from_solr($record->nid, 'node_search');
    }
  }
}

function _drush_node_search_remove_from_solr($nid, $func) {
  $node = new stdClass();
  $node->nid = $nid;
  if (apachesolr_delete_node_from_index($node)) {
    watchdog($func, 'WARNING: !nid removed from solr index', array('!nid' => $node->nid), WATCHDOG_WARNING);
  } else {
    watchdog($func, 'ERROR: Unables to remove !nid from solr index', array('!nid' => $node->nid), WATCHDOG_ERROR);
  }
}

function drush_node_search_add_nids_to_solr() {
  if (!$idlist = drush_get_option('idlist')) {
    drush_set_error(dt('idlist not specified.'));
    return FALSE;
  }
  $arr_idlist = explode(',', $idlist);
  if (!$arr_idlist) {
    drush_set_error(dt('idlist must be a comma-delimited list of node ids.'));
    return FALSE;
  }

  $result = db_select('node', 'n')
    ->fields('n', array('nid', 'changed'))
    ->condition('nid', $arr_idlist)
    ->execute();
  $row_count = $result->rowCount();

  if ($row_count) {
    drush_log(dt('Attempting to index @count to solr.', array('@count' => format_plural($row_count, 'one node', '@count nodes'))), 'ok');
  }
  else {
    drush_set_error(dt('None of the submitted node ids are valid node ids.'));
    return FALSE;
  }

  $rows = $result->fetchAll();
  try {
    $result = apachesolr_index_nodes($rows, 'apachesolr_search');
    drush_log(dt('Last change is now set to @lt and last nid is @ln', array('@lt' => date('r', $result['last_change']), '@ln' => $result['last_nid'])), 'success');
  }
  catch (Exception $e) {
    drush_set_error(dt($e->getMessage()));
  }
}
