<?php
/**
 * Search analtyics pages for ECL. Displaying following table:
 *  - per content type
 *      total number - total number have mslo - exlusive mslo - bs/ww - exlusive bs/ww - In search
 *  - Totals
 *      total number in ecl
 *      total number in apache
 *
 */
function node_search_get_overview() {
  $excluded_types = variable_get('apachesolr_search_excluded_types', array());
  $exclude_types = array_filter($excluded_types);
  $types = array_map('check_plain', node_get_types('names'));
  $result['total']['data']['type'] = 'TOTALS';
  foreach($types as $type => $readable){
    if(!in_array($type, $exclude_types)){
      $result[$type]['data']['type'] = $readable;
      $result[$type]['data']['total'] = node_search_get_totals($type);
      $result['total']['data']['total'] += $result[$type]['data']['total'];
    }
  }

  $output = theme('table', array('type', 'total nodes to index'), $result);
  return $output;
}


function node_search_get_totals($type = '') {
  $query = "SELECT COUNT(*) as count FROM {node} n WHERE type = '%s'";
  $result = db_fetch_object(db_query_slave($query, $type));
  return $result->count;
}

/**
 * Callback function for node search settings.
 */
function node_search_settings() {
  global $conf;
  $form['mslo_mslo_boost'] = array(
    '#type' => 'textfield',
    '#title' => t('Boost for MSLO content'),
    '#description' => t('Leave empty for no boost value...'),
    '#default_value' => variable_get('mslo_mslo_boost', ''),
  );

  $form['mslo_source_values'] = array(
    '#type' => 'textarea',
    '#title' => 'Source values',
    '#default_value' => variable_get('mslo_source_values', ''),
    '#description' => t('Give the exact values of the Martha stewart source. This is a comma separated list'),
  );

  $form['mslo_deault_search_string'] = array(
    '#type' => 'textfield',
    '#title' => 'Default search string',
    '#default_value' => variable_get('mslo_deault_search_string', 'Enter a search term'),
    '#description' => t('Default search text in the search box. Searches with this text get redirected to the no results page'),
  );

  $form['node_search_url_js'] = array(
    '#type' => 'textfield',
    '#title' => t('Node Search Server (JS Enabled)'),
    '#default_value' => $conf['node_search_js'],
    '#description' => t('The url of the node search server, starting with http.'),
    '#required' => TRUE,
    '#disabled' => TRUE,
  );

  $form['node_search_url_nojs'] = array(
    '#type' => 'textfield',
    '#title' => t('Node Search Server (JS Disabled)'),
    '#default_value' => $conf['node_search_nojs'],
    '#description' => t('The url of the node search server, starting with http.'),
    '#required' => TRUE,
    '#disabled' => TRUE,
  );

  return system_settings_form($form);
}

/**
 * Callback function for seasonal search settings.
 * @FIXME: Ported over from node_search module.  IDK if this is being used anymore.
 */
function node_seasonal_search_settings() {
  jquery_ui_add('ui.tabs');
  // @FIXME: Move JS into node search module.
  drupal_add_js(drupal_get_path('module', 'node_search') . '/js/mslo_search_admin.js', 'module', 'footer');
  drupal_add_css(libraries_get_path('jquery.ui') . '/themes/base/ui.tabs.css');
  drupal_add_css(drupal_get_path('module', 'node_search') . '/css/mslo_search_admin.css');

  // @FIXME: Move JS into node search module.
  $settings = variable_get('mslo_search_seasonal_taxons', array());
  $by_month = array_fill(1, 12, '');
  $by_term = array();
  foreach ($settings as $uuid => $months) {
    // @FIXME: This looks inefficient.
    $tid = mags_taxonomy_get_term_tid($uuid);
    $term = mags_taxonomy_get_term($tid);
    foreach ($months as $month) {
      $by_month[$month][] = $term->name;
      $by_term[trim($term->name)][] = date("F", mktime(0, 0, 0, $month, 10));
    }
  }

  unset($months);
  // @TODO: Make this themable output.
  $output = '<div id="tabs">';
  $links = array(l("By Month", "", array('fragment' => 'by-month', 'external' => TRUE)), l("By Term", "", array('fragment' => 'by-term', 'external' => TRUE)));
  $output .= '<ul>';
  foreach ($links as $link) {
    $output .= '<li>' . $link . '</li>';
  }
  $output .= '</ul>';
  $output .= '<div id="by-month">';
  foreach ($by_month as $month => $terms) {
    $month_name = date("F", mktime(0, 0, 0, $month, 10));
    $output .= theme('item_list', $terms, $month_name);
  }
  $output .= '</div>';
  $output .= '<div id="by-term">';
  ksort($by_term);
  foreach ($by_term as $term => $months) {
    $output .= theme('item_list', $months, $term);
  }
  return $output . '</div></div>';
}
