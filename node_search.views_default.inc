<?php

/**
 * Implementation of hook_views_default_views().
 */
function node_search_views_default_views() {
  $views = array();

$view = new view;
$view->name = 'solr_video_tout';
$view->description = 'Video touted at top of search results';
$view->tag = '';
$view->base_table = 'apachesolr_node';
$view->human_name = 'Solr Video Tout';
$view->core = 6;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Defaults */
$handler = $view->new_display('default', 'Defaults', 'default');
$handler->display->display_options['use_more'] = TRUE;
$handler->display->display_options['use_more_text'] = 'More Videos';
$handler->display->display_options['link_display'] = 'custom_url';
$handler->display->display_options['link_url'] = 'search/apachesolr_search/!1';
$handler->display->display_options['access']['type'] = 'none';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'some';
$handler->display->display_options['pager']['options']['items_per_page'] = '3';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['style_plugin'] = 'list';
$handler->display->display_options['style_options']['row_class'] = 'search-result-video';
$handler->display->display_options['style_options']['wrapper_class'] = 'feat-videos-container';
$handler->display->display_options['row_plugin'] = 'fields';
$handler->display->display_options['row_options']['hide_empty'] = 0;
$handler->display->display_options['row_options']['default_field_elements'] = 0;
/* Header: Global: Text area */
$handler->display->display_options['header']['area']['id'] = 'area';
$handler->display->display_options['header']['area']['table'] = 'views';
$handler->display->display_options['header']['area']['field'] = 'area';
$handler->display->display_options['header']['area']['label'] = '';
$handler->display->display_options['header']['area']['empty'] = FALSE;
$handler->display->display_options['header']['area']['content'] = 'Top Video Results';
$handler->display->display_options['header']['area']['tokenize'] = 0;
/* Field: Nid */
$handler->display->display_options['fields']['nid']['id'] = 'nid';
$handler->display->display_options['fields']['nid']['table'] = 'apachesolr_node_node';
$handler->display->display_options['fields']['nid']['field'] = 'nid';
$handler->display->display_options['fields']['nid']['ui_name'] = 'Nid';
$handler->display->display_options['fields']['nid']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['nid']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['nid']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['nid']['alter']['external'] = 0;
$handler->display->display_options['fields']['nid']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['nid']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['nid']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['nid']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['nid']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['nid']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['nid']['alter']['trim'] = 0;
$handler->display->display_options['fields']['nid']['alter']['html'] = 0;
$handler->display->display_options['fields']['nid']['element_label_colon'] = 1;
$handler->display->display_options['fields']['nid']['element_default_classes'] = 1;
$handler->display->display_options['fields']['nid']['hide_empty'] = 0;
$handler->display->display_options['fields']['nid']['empty_zero'] = 0;
$handler->display->display_options['fields']['nid']['hide_alter_empty'] = 1;
$handler->display->display_options['fields']['nid']['link_to_node'] = 0;
/* Field: Path */
$handler->display->display_options['fields']['path']['id'] = 'path';
$handler->display->display_options['fields']['path']['table'] = 'apachesolr_node_node';
$handler->display->display_options['fields']['path']['field'] = 'path';
$handler->display->display_options['fields']['path']['ui_name'] = 'Path';
$handler->display->display_options['fields']['path']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['path']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['path']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['path']['alter']['external'] = 0;
$handler->display->display_options['fields']['path']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['path']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['path']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['path']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['path']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['path']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['path']['alter']['trim'] = 0;
$handler->display->display_options['fields']['path']['alter']['html'] = 0;
$handler->display->display_options['fields']['path']['element_type'] = '0';
$handler->display->display_options['fields']['path']['element_label_colon'] = 1;
$handler->display->display_options['fields']['path']['element_wrapper_type'] = '0';
$handler->display->display_options['fields']['path']['element_default_classes'] = 1;
$handler->display->display_options['fields']['path']['hide_empty'] = 0;
$handler->display->display_options['fields']['path']['empty_zero'] = 0;
$handler->display->display_options['fields']['path']['hide_alter_empty'] = 1;
$handler->display->display_options['fields']['path']['absolute'] = 1;
/* Field: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'apachesolr_node_node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['ui_name'] = 'Title';
$handler->display->display_options['fields']['title']['label'] = '';
$handler->display->display_options['fields']['title']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['title']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['title']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['title']['alter']['external'] = 0;
$handler->display->display_options['fields']['title']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['title']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['title']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['title']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['title']['alter']['trim'] = 0;
$handler->display->display_options['fields']['title']['alter']['html'] = 0;
$handler->display->display_options['fields']['title']['element_label_colon'] = 1;
$handler->display->display_options['fields']['title']['element_default_classes'] = 1;
$handler->display->display_options['fields']['title']['hide_empty'] = 0;
$handler->display->display_options['fields']['title']['empty_zero'] = 0;
$handler->display->display_options['fields']['title']['hide_alter_empty'] = 1;
$handler->display->display_options['fields']['title']['link_to_node'] = 1;
/* Field: Short Description */
$handler->display->display_options['fields']['field_short_description_value']['id'] = 'field_short_description_value';
$handler->display->display_options['fields']['field_short_description_value']['table'] = 'apachesolr_node_node_data_field_short_description';
$handler->display->display_options['fields']['field_short_description_value']['field'] = 'field_short_description_value';
$handler->display->display_options['fields']['field_short_description_value']['ui_name'] = 'Short Description';
$handler->display->display_options['fields']['field_short_description_value']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['field_short_description_value']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['field_short_description_value']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['field_short_description_value']['alter']['external'] = 0;
$handler->display->display_options['fields']['field_short_description_value']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['field_short_description_value']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['field_short_description_value']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['field_short_description_value']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['field_short_description_value']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['field_short_description_value']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['field_short_description_value']['alter']['trim'] = 0;
$handler->display->display_options['fields']['field_short_description_value']['alter']['html'] = 0;
$handler->display->display_options['fields']['field_short_description_value']['element_label_colon'] = 1;
$handler->display->display_options['fields']['field_short_description_value']['element_default_classes'] = 1;
$handler->display->display_options['fields']['field_short_description_value']['hide_empty'] = 0;
$handler->display->display_options['fields']['field_short_description_value']['empty_zero'] = 0;
$handler->display->display_options['fields']['field_short_description_value']['hide_alter_empty'] = 1;
$handler->display->display_options['fields']['field_short_description_value']['link_to_node'] = 0;
$handler->display->display_options['fields']['field_short_description_value']['label_type'] = 'none';
/* Argument: Apache Solr: Search */
$handler->display->display_options['arguments']['text']['id'] = 'text';
$handler->display->display_options['arguments']['text']['table'] = 'apachesolr_node';
$handler->display->display_options['arguments']['text']['field'] = 'text';
$handler->display->display_options['arguments']['text']['default_action'] = 'default';
$handler->display->display_options['arguments']['text']['style_plugin'] = 'default_summary';
$handler->display->display_options['arguments']['text']['default_argument_type'] = 'context';
$handler->display->display_options['arguments']['text']['default_argument_options']['namespace'] = 'solr';
$handler->display->display_options['arguments']['text']['default_argument_options']['attribute'] = 'keys';
$handler->display->display_options['arguments']['text']['default_argument_skip_url'] = 0;
$handler->display->display_options['arguments']['text']['break_phrase'] = 0;
$handler->display->display_options['arguments']['text']['not'] = 0;
/* Filter: Apache Solr: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'apachesolr_node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'video' => 'video',
);
/* Filter: Apache Solr: Published */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'apachesolr_node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = '1';
/* Filter: Apache Solr: Search */
$handler->display->display_options['filters']['text']['id'] = 'text';
$handler->display->display_options['filters']['text']['table'] = 'apachesolr_node';
$handler->display->display_options['filters']['text']['field'] = 'text';

/* Display: Block */
$handler = $view->new_display('block', 'Block', 'block');
$handler->display->display_options['link_url'] = 'search/apachesolr_search/!1';

/* Display: Playlist Block */
$handler = $view->new_display('block', 'Playlist Block', 'playlist_block');
$handler->display->display_options['display_description'] = 'This block appears on the video standalone page';
$handler->display->display_options['defaults']['use_more'] = FALSE;
$handler->display->display_options['defaults']['pager'] = FALSE;
$handler->display->display_options['pager']['type'] = 'some';
$handler->display->display_options['pager']['options']['items_per_page'] = '23';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['defaults']['style_plugin'] = FALSE;
$handler->display->display_options['style_plugin'] = 'list';
$handler->display->display_options['style_options']['row_class'] = 'search-result-video';
$handler->display->display_options['style_options']['class'] = 'clearfix';
$handler->display->display_options['style_options']['wrapper_class'] = 'feat-videos-container';
$handler->display->display_options['defaults']['style_options'] = FALSE;
$handler->display->display_options['defaults']['row_plugin'] = FALSE;
$handler->display->display_options['row_plugin'] = 'fields';
$handler->display->display_options['row_options']['hide_empty'] = 0;
$handler->display->display_options['row_options']['default_field_elements'] = 0;
$handler->display->display_options['defaults']['row_options'] = FALSE;
$handler->display->display_options['defaults']['header'] = FALSE;
/* Header: Global: Text area */
$handler->display->display_options['header']['area']['id'] = 'area';
$handler->display->display_options['header']['area']['table'] = 'views';
$handler->display->display_options['header']['area']['field'] = 'area';
$handler->display->display_options['header']['area']['label'] = '';
$handler->display->display_options['header']['area']['empty'] = FALSE;
$handler->display->display_options['header']['area']['content'] = 'Related videos';
$handler->display->display_options['header']['area']['tokenize'] = 0;
$handler->display->display_options['defaults']['fields'] = FALSE;
/* Field: Path */
$handler->display->display_options['fields']['path']['id'] = 'path';
$handler->display->display_options['fields']['path']['table'] = 'apachesolr_node_node';
$handler->display->display_options['fields']['path']['field'] = 'path';
$handler->display->display_options['fields']['path']['ui_name'] = 'Path';
$handler->display->display_options['fields']['path']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['path']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['path']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['path']['alter']['external'] = 0;
$handler->display->display_options['fields']['path']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['path']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['path']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['path']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['path']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['path']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['path']['alter']['trim'] = 0;
$handler->display->display_options['fields']['path']['alter']['html'] = 0;
$handler->display->display_options['fields']['path']['element_type'] = '0';
$handler->display->display_options['fields']['path']['element_label_colon'] = 1;
$handler->display->display_options['fields']['path']['element_wrapper_type'] = '0';
$handler->display->display_options['fields']['path']['element_default_classes'] = 1;
$handler->display->display_options['fields']['path']['hide_empty'] = 0;
$handler->display->display_options['fields']['path']['empty_zero'] = 0;
$handler->display->display_options['fields']['path']['hide_alter_empty'] = 1;
$handler->display->display_options['fields']['path']['absolute'] = 1;
/* Field: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'apachesolr_node_node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['ui_name'] = 'Title';
$handler->display->display_options['fields']['title']['label'] = '';
$handler->display->display_options['fields']['title']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['title']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['title']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['title']['alter']['external'] = 0;
$handler->display->display_options['fields']['title']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['title']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['title']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['title']['alter']['max_length'] = '28';
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['title']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['title']['alter']['trim'] = 1;
$handler->display->display_options['fields']['title']['alter']['html'] = 0;
$handler->display->display_options['fields']['title']['element_label_colon'] = 1;
$handler->display->display_options['fields']['title']['element_default_classes'] = 1;
$handler->display->display_options['fields']['title']['hide_empty'] = 0;
$handler->display->display_options['fields']['title']['empty_zero'] = 0;
$handler->display->display_options['fields']['title']['hide_alter_empty'] = 1;
$handler->display->display_options['fields']['title']['link_to_node'] = 1;
$handler->display->display_options['defaults']['arguments'] = FALSE;
/* Argument: Apache Solr: More Like This Node id */
$handler->display->display_options['arguments']['mlt']['id'] = 'mlt';
$handler->display->display_options['arguments']['mlt']['table'] = 'apachesolr_node';
$handler->display->display_options['arguments']['mlt']['field'] = 'mlt';
$handler->display->display_options['arguments']['mlt']['default_action'] = 'default';
$handler->display->display_options['arguments']['mlt']['style_plugin'] = 'default_summary';
$handler->display->display_options['arguments']['mlt']['default_argument_type'] = 'context';
$handler->display->display_options['arguments']['mlt']['default_argument_options']['namespace'] = 'node';
$handler->display->display_options['arguments']['mlt']['default_argument_options']['attribute'] = 'nid';
$handler->display->display_options['arguments']['mlt']['default_argument_skip_url'] = 0;
$handler->display->display_options['arguments']['mlt']['mlt_fl'] = array(
  'title' => '1.0',
  'taxonomy_names' => '1.0',
  'body' => '0',
  'name' => '0',
  'path_alias' => '0',
  'tm_mslo_tags' => '0',
  'tm_node' => '0',
  'ts_referenced_images' => '0',
  'ts_vid_1_names' => '0',
  'ts_vid_2_names' => '0',
  'ts_vid_3_names' => '0',
  'ts_vid_4000_names' => '0',
  'ts_vid_4_names' => '0',
  'ts_vid__names' => '0',
);

/* Display: Most Recent Videos */
$handler = $view->new_display('block', 'Most Recent Videos', 'recent_videos');
$handler->display->display_options['defaults']['use_more'] = FALSE;
$handler->display->display_options['defaults']['pager'] = FALSE;
$handler->display->display_options['pager']['type'] = 'some';
$handler->display->display_options['pager']['options']['items_per_page'] = '23';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['defaults']['header'] = FALSE;
/* Header: Global: Text area */
$handler->display->display_options['header']['area']['id'] = 'area';
$handler->display->display_options['header']['area']['table'] = 'views';
$handler->display->display_options['header']['area']['field'] = 'area';
$handler->display->display_options['header']['area']['label'] = '';
$handler->display->display_options['header']['area']['empty'] = FALSE;
$handler->display->display_options['header']['area']['content'] = 'Recent Videos';
$handler->display->display_options['header']['area']['tokenize'] = 0;
$handler->display->display_options['defaults']['sorts'] = FALSE;
/* Sort criterion: Apache Solr: Updated date */
$handler->display->display_options['sorts']['changed']['id'] = 'changed';
$handler->display->display_options['sorts']['changed']['table'] = 'apachesolr_node';
$handler->display->display_options['sorts']['changed']['field'] = 'changed';
$handler->display->display_options['defaults']['arguments'] = FALSE;

  $views[$view->name] = $view;

  return $views;
}
