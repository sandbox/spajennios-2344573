
<div class="search-content" id="js-search-wrap">
  <?php /* Search results js will replace this loader. */ ?>
  <span id="js-load-initial" class="loader-large"></span>
</div>


<!-- Wrapping Template for MS-->
<script id="search-template" type="text/x-handlebars-template">
  {{#if contentArr}}
    {{#if firstPage}}
    <div class="search-header clear-fix">
      <div class="search-results-title">Search Results </div>
      {{#if searchTermDecoded}}
        <p class="search-count">
          {{numResults}} Results for "<span class="page-searched-tag">{{searchTermDecoded}}</span>" {{selectedFilter}}
        </p>
      {{/if}}
      <div class="li-filter om-links" data-om-title="search_filters">
        <div class="item-list">
          <ul id="search-filters">
            <li class="first item-odd" data-content='all'>
              <a href="?keys={{searchTerm}}">
                <span class="filter-link">All</span>
              </a>
            </li>
            {{#filters}}
              {{> filters}}
            {{/filters}}
            </ul>
        </div>
        <span class="refine-label">View:</span>
      </div>
    </div>
    {{/if}}
    {{#if firstPage}}
    <ul class="search-results clearfix" id="js-search-results">
    {{/if}}
      {{#contentArr}}
        {{> insides}}
      {{/contentArr}}
    {{#if firstPage}}
    </ul>
    {{/if}}
    {{#if nextPageNum}}
      <div class="search-msg search-see-more header-b" id="js-search-pager">
        <a href="#search{{nextPageNum}}" id="js-search-next" data-count="{{currentPageNum}}" data-content="{{nextPageUrl}}">See More <span class="glyph gl-down-dbl-arw"></span></a>
      </div>
    {{/if}}
  {{else}}
    <div class="search-results-title no-results">Search Results </div>
    <div class="no-search-results-page clear-fix">
      <div class="no-results">
       <h1 class="title">No results found.</h1>
       <p>We are sorry, we did not find any search results for <strong>{{searchTerm}}</strong> {{selectedFilter}} .
       {{#if spellcheck}}
         Did you mean
         <a href="?keys={{spellcheck}}"><strong>{{spellcheck}}</strong></a>
         ?
       {{/if}}
       </p>
       {{{contentList}}}
      </div>
    </div>
  {{/if}}
</script>

<!-- Wrapping Template for MSW-->
<script id="search-template-msw" type="text/x-handlebars-template">
  {{#if contentArr}}
    {{#if firstPage}}
    <div class="search-header clear-fix">
      <div class="search-results-title">Search Results </div>
      {{#if searchTermDecoded}}
        <p class="search-count">
          {{numResults}} Results for "<span class="page-searched-tag">{{searchTermDecoded}}</span>" {{selectedFilter}}
        </p>
      {{/if}}
    </div>
    {{/if}}
    {{#if firstPage}}
    <ul class="search-results clearfix" id="js-search-results">
    {{/if}}
      {{#contentArr}}
        {{> insides}}
      {{/contentArr}}
    {{#if firstPage}}
    </ul>
    {{/if}}
    {{#if nextPageNum}}
      <div class="search-msg search-see-more header-b" id="js-search-pager">
        <a href="#search{{nextPageNum}}" id="js-search-next" data-count="{{currentPageNum}}" data-content="{{nextPageUrl}}">See More <span class="glyph gl-down-dbl-arw"></span></a>
      </div>
    {{/if}}
  {{else}}
    <div class="search-results-title no-results">Search Results </div>
    <div class="no-search-results-page clear-fix">
      <div class="no-results">
       <h1 class="title">No results found.</h1>
       <p>We are sorry, we did not find any search results for <strong>{{searchTerm}}</strong> {{selectedFilter}} .
       {{#if spellcheck}}
         Did you mean
         <a href="?keys={{spellcheck}}"><strong>{{spellcheck}}</strong></a>
         ?
       {{/if}}
       </p>
       {{{contentList}}}
      </div>
    </div>
  {{/if}}
</script>

<!-- Each filter -->
<script id="search-template-partial-filters" type="text/x-handlebars-template">
  <li data-content='{{type}}'>
     <a href="?keys={{searchTerm}}&filters={{type}}">
       <span class="filter-link">{{display}}</span>
     </a>
  </li>
</script>

<!-- Singular Piece of Content Template -->
<script id="search-template-partial" type="text/x-handlebars-template">
  {{#if isAd}}
    {{> ad}}
  {{else}}
    <li class="node-result js-search-item om-section" data-om-title="search_result">
      <div class="search-content-container">
        <div class="simage-wrap js-search-image">
          <a href="{{domain}}/{{pathAlias}}" data-om-url="{{pathAlias}}">
          {{#if imageUrl}}
            <img class="simage simage-{{type}}" src="{{imageUrl}}" data-original="{{imageUrl}}" alt="{{title}}" title="{{title}}">
          {{/if}}
          {{#if videoGlyph}}
            {{#if imageUrl}}
              <span class="glyph glyph-overlay gl-play"></span>
            {{/if}}
          {{/if}}
          </a>
        </div>
        <div class="box-body">
          <div class="box-content">
            {{#if sponsored}}
              <div class="banner">
                <span class="banner-content">sponsored</span>
              </div>
            {{else}}
              {{#if badge}}
                <h4 class="badge-label badge-type-{{badge_code}}">
                  <span class="glyph gl-arrow"></span>{{badge_desc}}
                </h4>
              {{else}}
                <div class="tout-title-tiny first">{{typeName}}</div>
              {{/if}}
            {{/if}}
            <h2 class="title">
              <a href="{{domain}}/{{pathAlias}}" data-om-url="{{imageUrl}}">{{titleEncoded}}</a>
            </h2>
            <div class="search-more-like">
              <a href="/search/results/mlt?nid={{nid}}&keys={{searchTerm}}">More Like This</a>
            </div>
          </div>
        </div>
      </div>
    </li>
  {{/if}}
</script>

<!-- Ad Content Template -->
<script id="search-template-partial-ad" type="text/x-handlebars-template">
  <div class="js-search-item block ad-block-search ad-block dart-tag-27">
    <div class="ad-iframe-wrap">
      <div class="ad-label">
        Advertisement
      </div>
      <iframe
        frameborder="0"
        scrolling="no"
        width="300"
        height="250"
        src=""
        data-adsrc="{{adSrc}}"
        class="{{adClass}}"
        data-adid="{{adDataAdid}}">
      </iframe>
    </div>
  </div>
</script>
